#include <math.h>
int sensorPin = A5;    // select the input pin for the potentiometer
int sensorValue = 0;  // variable to store the value coming from the sensor
int sensorVCC = 2;   //Transistor
float humidity = 0.0;  //Humidity percentage
int Relay3 =  A2;     // Analog input

void setup() {
  //Setting up the pins as digital output
  pinMode(Relay3, OUTPUT);
  pinMode(sensorVCC, OUTPUT);
  digitalWrite(sensorVCC, LOW);
  digitalWrite(Relay3, LOW);
  Serial.begin(9600);
}

void water() {
    digitalWrite(Relay3, HIGH);
    while (humidity <= 35)
    {
      digitalWrite(sensorVCC, HIGH);
      sensorValue = analogRead(sensorPin);
      humidity = (sensorValue * 100.0) / 1024.0;
      digitalWrite(sensorVCC, LOW);
      
      Serial.print("data = " );                       
      Serial.println(sensorValue);
      Serial.print(humidity);
      Serial.println("% humidity");
      delay(3000);
    }
    digitalWrite(Relay3, LOW);
  }


void loop() {
  //Power the sensor
  digitalWrite(sensorVCC, HIGH);
  delay(100);
  // read the value from the sensor:
  sensorValue = analogRead(sensorPin);
  humidity = (sensorValue * 100.0) / 1024.0;
  //Stop the power
  digitalWrite(sensorVCC, LOW);
  if (humidity < 25) water();
  
  
  Serial.print("data = " );                       
  Serial.println(sensorValue);
  Serial.print(humidity);
  Serial.println("% humidity");
  Serial.print("Waiting..." );
  Serial.print(" " );
  
  //Wait 1 minutes
  delay(60 * 1000); 
}
